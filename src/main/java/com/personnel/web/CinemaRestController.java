package com.personnel.web;

import com.personnel.dao.IFilmRepository;
import com.personnel.dao.ITicketRepository;
import com.personnel.entities.Film;
import com.personnel.entities.Ticket;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("*")
public class CinemaRestController {

    @Autowired
    private IFilmRepository iFilmRepository;
    @Autowired
    private ITicketRepository iTicketRepository;


    @GetMapping(path = "/imageFilm/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public  byte[] image (@PathVariable(name = "id")Long id) throws IOException {
        Film film = iFilmRepository.findById(id).get();
        String photoName = film.getPhoto();
        File file = new File(System.getProperty("user.home")+"/imgcinema/"+photoName);
        Path path = Paths.get(file.toURI());
        return Files.readAllBytes(path);
    }

    @PostMapping("/payerTickets")
    @Transactional
    public List<Ticket> payerTickets(@RequestBody TicketForm ticketForm) {
    List<Ticket> listTickets = new ArrayList<>();
    ticketForm.getTickets().forEach(idTicket->{
        Ticket ticket = iTicketRepository.findById(idTicket).get();
        ticket.setNomClient(ticketForm.getNomClient());
        ticket.setReserve(true);
        ticket.setCodePayment(ticketForm.getCodePayement());
        iTicketRepository.save(ticket);
        listTickets.add(ticket);

    });
    return listTickets;
    }
}

@Data
class TicketForm {
    private String nomClient;
    private int codePayement;
    private List<Long> tickets = new ArrayList<>();
        }

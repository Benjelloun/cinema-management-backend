package com.personnel.entities;

import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;
import java.util.Date;

@Projection(name = "p1", types = com.personnel.entities.Projection.class)
public interface IProjectionProj {

    public Long getId();
    public Date getDateProjection();
    public double getPrix();
    public Film getFilm();
    public Salle getSalle();
    public Seance getSeance();
    public Collection<Ticket> getTickets();
}

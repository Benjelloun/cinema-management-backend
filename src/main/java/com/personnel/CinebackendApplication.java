package com.personnel;

import com.personnel.dao.ISalleRepository;
import com.personnel.entities.Film;
import com.personnel.service.ICinemaInitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class CinebackendApplication implements CommandLineRunner {
    @Autowired
    private ICinemaInitService iCinemaInitService;

    @Autowired
    private RepositoryRestConfiguration repositoryRestConfiguration;

    public static void main(String[] args) {

        SpringApplication.run(CinebackendApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        repositoryRestConfiguration.exposeIdsFor(Film.class);
    iCinemaInitService.initVilles();
    iCinemaInitService.initCinemas();
    iCinemaInitService.initSalles();
    iCinemaInitService.initPlaces();
    iCinemaInitService.initSeances();
    iCinemaInitService.initCategories();
    iCinemaInitService.initFilms();
    iCinemaInitService.initProjections();
    iCinemaInitService.initTickets();
    }
}

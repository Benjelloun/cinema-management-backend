package com.personnel.service;

import com.personnel.dao.*;
import com.personnel.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Service
@Transactional
public class CinemaInitServiceImpl implements ICinemaInitService {

    @Autowired
    private IVilleRepository iVilleRepository;
    @Autowired
    private ICinemaRepository iCinemaRepository;
    @Autowired
    private ISalleRepository iSalleRepository;
    @Autowired
    private IPlaceRepository iPlaceRepository;
    @Autowired
    private ISeanceRepository iSeanceRepository;
    @Autowired
    private IFilmRepository iFilmRepository;
    @Autowired
    private IProjectionRepository iProjectionRepository;
    @Autowired
    private ITicketRepository iTicketRepository;
    @Autowired
    private ICategorieRepository iCategorieRepository;

    @Override
    public void initVilles() {
    Stream.of("PARIS","BERLIN","CASABLANCA").forEach(nameVille->{
        Ville ville = new Ville();
        ville.setName(nameVille);
        iVilleRepository.save(ville);
    });
    }

    @Override
    public void initCinemas() {
    iVilleRepository.findAll().forEach(ville -> {
        Stream.of("UGC","PATHE","MEGARAMA","GOUMANT","CGR").forEach(nameCinema->{
            Cinema cinema = new Cinema();
            cinema.setName(nameCinema);
            cinema.setVille(ville);
            cinema.setNombreSalles(3+(int)Math.random()*7);
            iCinemaRepository.save(cinema);
        });
    });
    }

    @Override
    public void initSalles() {
    iCinemaRepository.findAll().forEach(cinema -> {
        for (int i = 0; i <cinema.getNombreSalles() ; i++) {
            Salle salle = new Salle();
            salle.setName("salle"+(i+1));
            salle.setCinema(cinema);
            salle.setNombrePlace(15+(int)Math.random()*20);
            iSalleRepository.save(salle);
        }
    });
    }

    @Override
    public void initPlaces() {
    iSalleRepository.findAll().forEach(salle -> {
        for (int i = 0; i <salle.getNombrePlace() ; i++) {
            Place place = new Place();
            place.setNumeroPlace(i+1);
            place.setSalle(salle);
            iPlaceRepository.save(place);
        }
    });
    }

    @Override
    public void initSeances() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Stream.of("12:00","15:00","17:00","19:00","21:00").forEach(s->{
            Seance seance = new Seance();
            try {
                seance.setHeureDebut(dateFormat.parse(s));
                iSeanceRepository.save(seance);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void initCategories() {
        Stream.of("Aventure","DRAMA","WAR","histoire","Comedy").forEach(cat->{
            Categorie categorie = new Categorie();
            categorie.setName(cat);
            iCategorieRepository.save(categorie);
        });

    }

    @Override
    public void initFilms() {
        double[] durees = new double[]{1,1.5,2,2.5,3,3.5};
        List<Categorie> categories= iCategorieRepository.findAll();
        Stream.of("Game Of Thrones","les 300","USS ALABAMA","CASANEGRA","EQUALIZER").forEach(titre->{
            Film film = new Film();
            film.setTitre(titre);
            film.setDuree(durees[new Random().nextInt(durees.length)]);
            film.setPhoto(titre.replaceAll(" ", "")+".jpg");
            film.setCategorie(categories.get(new Random().nextInt(categories.size())));
            iFilmRepository.save(film);

        });
    }

    @Override
    public void initProjections() {
        double [] price= new double[] {30,50,60,70,90,100};
    iVilleRepository.findAll().forEach(ville -> {
        ville.getCinemas().forEach(cinema -> {
            cinema.getSalles().forEach(salle -> {
                iFilmRepository.findAll().forEach(film -> {
                    iSeanceRepository.findAll().forEach(seance -> {
                        Projection projection = new Projection();
                        projection.setDateProjection(new Date());
                        projection.setFilm(film);
                        projection.setPrix(price[new Random().nextInt(price.length)]);
                        projection.setSalle(salle);
                        projection.setSeance(seance);
                        iProjectionRepository.save(projection);
                    });
                });
            });
        });
    });
    }

    @Override
    public void initTickets() {
    iProjectionRepository.findAll().forEach(p -> {
        p.getSalle().getPlaces().forEach(place -> {
            Ticket ticket = new Ticket();
            ticket.setPlace(place);
            ticket.setPrix(p.getPrix());
            ticket.setProjection(p);
            ticket.setReserve(false);
            iTicketRepository.save(ticket);
        });
    });
    }
}

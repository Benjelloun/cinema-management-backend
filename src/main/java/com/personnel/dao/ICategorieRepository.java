package com.personnel.dao;

import com.personnel.entities.Categorie;
import com.personnel.entities.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")

public interface ICategorieRepository extends JpaRepository<Categorie,Long> {
}

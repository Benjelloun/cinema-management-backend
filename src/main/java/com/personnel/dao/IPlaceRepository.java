package com.personnel.dao;

import com.personnel.entities.Cinema;
import com.personnel.entities.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")

public interface IPlaceRepository extends JpaRepository<Place,Long> {
}
